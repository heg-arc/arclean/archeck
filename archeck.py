#!/usr/bin/env python
# coding: utf-8
import os
from dotenv import load_dotenv
import logging
import requests
from pysnmp.hlapi import getCmd, CommunityData, UdpTransportTarget, ObjectType, ObjectIdentity, SnmpEngine, ContextData


load_dotenv()
logger = logging.getLogger(__name__)

username = os.getenv("API_USER")
password = os.getenv("API_PASSWORD")
login_url = os.getenv("LOGIN_URL")
api = os.getenv("API_URL")

session = requests.Session()


def authentication():
    initial_response = session.get(login_url)
    csrf_token = initial_response.cookies['csrftoken']
    
    login_response = session.post(login_url, data={'login': username, 'password': password, 'csrfmiddlewaretoken': csrf_token})
    if login_response.status_code == 200:
        return True
    else:
        logging.error(f"Login failed. Status code: {login_response.status_code}")
    return None


def retrieve_configuration():
    response = session.get(api)
    if response.status_code == 200:
        json_response = response.json()
        if "printers" in json_response:
            return json_response['printers']
    else:
        logger.error(f"Unable to retrieve configuration. Error code: {response.status_code}")
    return None


def get_printer_status(printer):
    data = {}
    object_types = [ObjectType(ObjectIdentity(probe['oid'])) for probe in printer['probes']]

    error_indication, error_status, error_index, var_binds = next(
        getCmd(SnmpEngine(),
            CommunityData(printer['snmp_community_string']),
            UdpTransportTarget((printer['ip_address'], 161)),
            ContextData(),
            *object_types)
        )
    
    if error_indication:
        logger.error(f"Error: {error_indication}")
    elif error_status:
        logger.error(f"Error at {error_index}: {error_status.prettyPrint()}")
    else:
        for i, var in enumerate(var_binds):
            value = var.prettyPrint().split("=")[1].strip()
            data[f"printer-{printer['pk']}-probe-{printer['probes'][i]['pk']}"] = value
        return data
    return None


def send_printer_statuses(statuses):
    response = session.post(api, data=statuses, headers={'X-CSRFToken': session.cookies['csrftoken']})
    if response.status_code == 200:
        logger.info(f"Success, statuses updated")
    else:
        logger.error(f"Unable to send statuses. Error code: {response.status_code}\n{statuses}")


def main():
    logger.info("Starting printers update")
    authentication()
    printers = retrieve_configuration()
    if printers:
        for printer in printers:
            statuses = get_printer_status(printer)
            if statuses:
                send_printer_statuses(statuses)
            else:
                logger.error(f"No status for printer <id = {printer['pk']} - {printer['ip_address']}>")
    else:
        logger.error("No authentication, aborting...")


if __name__ == "__main__":
    main()
