# Archeck

Small application to check the status of connected devices

License: GPLv3

## Settings

Install in a virtual environment:

    $ python3 -m venv venv
    $ source venv/bin/activate
    $ pip install requirements.txt

### Environment variables

Set the environment variables in the `.env` file:

    $ cp .env.template .env

## Usage

Run the script:

    $ python3 archeck.py

You better use a cron job to run the script every n minutes:

    $ crontab -e
    */10 * * * * /path/to/venv/bin/python3 /path/to/venv/bin/archeck.py

### API configuration

The API returns a JSON configuration containing all objects with their IP addresses and, for each object, a list of probes (OIDs) for which to retrieve the state value.


```json
{
   "printers":[
      {
         "pk":1,
         "ip_address":"157.16.20.21",
         "snmp_community_string":"public",
         "probes":[
            {
               "pk":1,
               "oid":"1.6.3"
            },
            {
               "pk":2,
               "oid":"1.6.3.1"
            },
            {
               "pk":3,
               "oid":"1.6.3.1.1"
            },
            {
               "pk":4,
               "oid":"1.6.3.2"
            }
         ]
      },
      {
         "pk":2,
         "ip_address":"192.168.5.14",
         "snmp_community_string":"public",
         "probes":[
            {
               "pk":1,
               "oid":"1.6.3"
            },
            {
               "pk":2,
               "oid":"1.6.3.1"
            },
            {
               "pk":3,
               "oid":"1.6.3.1.1"
            },
            {
               "pk":4,
               "oid":"1.6.3.2"
            }
         ]
      }
   ]
}
```
